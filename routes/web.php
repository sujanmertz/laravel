<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('back.layouts.app');
// });

use Illuminate\Support\Facades\Route;

Route::get('/', function () { return redirect()->intended('admin'); });

Route::group(['namespace' => 'Back'], function () {
    Route::get('login', 'UserController@login')->name('login');
    Route::post('login', 'UserController@loginAction');
});

Route::group(['namespace' => 'Back', 'prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('', 'DashboardController@index')->name('admin');
    
    Route::get('change-password','UserController@setting');
    Route::post('change-password','UserController@changePassword')->name('change-password');

    Route::get('tags','TagController@index')->name('tags');

    Route::get('add-tag', 'TagController@showTag');
    Route::post('add-tag','TagController@addTagAction')->name('add-tags');

    Route::get('deleteRow', 'TagController@delete');
    Route::post('deleteRow', 'TagController@delete')->name('delete');

    // Route::get('deleteRow/{id}', 'TagController@delete');
    // Route::post('deleteRow/{id}', 'TagController@delete')->name('delete');

    Route::get('edit-tag/{id}', 'TagController@editTag')->name('edit');
    Route::post('edit-tag', 'TagController@editTagAction')->name('edit-tag');

    Route::resource('blog', 'BlogController');

    Route::get('blogs','BlogController@index')->name('blogs');

    Route::get('add-blog', 'BlogController@create');
    Route::post('add-blog','BlogController@store')->name('add-blog');

    Route::get('deleteRow/{id}', 'BlogController@destroy');
    Route::post('deleteRow/{id}', 'BlogController@destroy')->name('blog.delete');

    Route::get('appointments', 'AppointmentController@index')->name('appointments');
    Route::resource('appointment', 'AppointmentController');

    Route::post('ajax', 'AppointmentController@getAjaxTimeSlot');




//     Route::group(['prefix' => 'user', 'middleware' => 'status'], function () {
// //        , 'middleware' => 'status'
//         Route::get('/', 'UserController@index')->name('user');

//         Route::get('add', 'UserController@addUser')->name('add-user');
//         Route::post('add', 'UserController@addUserAction');

//         Route::get('updateStatus', 'UserController@index')->name('updateStatus');
//         Route::post('updateStatus', 'UserController@updateStatus');

//         Route::get('deleteRow', 'UserController@index')->name('delete');
//         Route::post('deleteRow', 'UserController@delete');

       //Route::delete('delete');


//     });

    Route::get('login', 'UserController@logOut')->name('logout');
});
