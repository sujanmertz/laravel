/**
 * Created by Mertz~PC on 5/31/2017.
 */

$(document).ready(function(){
    $('.menu ul li.drop-down > a').on('click',function(e){
        e.preventDefault();
        $(this.nextElementSibling).slideToggle(300);
    });
});