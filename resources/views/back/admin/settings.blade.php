@extends('back.layouts.app')

@section('content')

<div class="content">

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <hr>
            <h1><i class="glyphicon glyphicon-lock"></i> Update Password</h1>
            <hr>

            <form action="{{route('change-password')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                {{--dd($users)--}}

                <input type="hidden" name="id" value="{{$users->id}}">

                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pass">Old Password</label>
                        <input type="password" name="opassword" id="opass" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="pass">New Password</label>
                        <input type="password" name="password" id="pass" class="form-control">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="cpass">Confirm Password</label>
                        <input type="password" name="password_confirmation" id="cpass" class="form-control">
                    </div>
                </div>
                <div class="col-md-6">

                    <button type="submit" name="updatePassword" class="btn btn-primary">
                        <i class="glyphicon glyphicon-cog"></i> Update Password
                    </button>

                </div>

            </form>
        </div>


    </div>


</div>

@endsection