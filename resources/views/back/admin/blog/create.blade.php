@extends('back.layouts.app')

@section('content')

<div class="content">

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <h1>&nbsp;Add New Blog</h1>
            <hr>
            {!! Form::open(['method'=>'post', 'route'=>'add-blog', 'class'=>'form-horizontal', 'files'=>'true']) !!}
            <div class="form-group">
                {!! Form::label('title', 'Title', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'id' => 'title']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('slug', 'Slug', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('slug', old('slug'), ['class' => 'form-control', 'id' => 'slug', 'readonly']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('content', 'Content', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('content', old('content'), ['class' => 'form-control', 'rows' => '3']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('image', 'Image', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::file('image', ['class' => 'form-control', 'id' => 'upload']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('description', 'Description', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::textarea('description', old('description'), ['class' => 'form-control summernote']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('tags', 'Tags', ['class' => 'col-sm-2 control-panel', 'style' => 'text-align:right' ]) !!}
                <div class="col-sm-6">
                    {!! Form::select('tags[]', $tags, old('tags'), ['class' => 'form-control select2', 'multiple' => 'multiple', 'required' => '']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    {!! Form::submit('Add Blog', ['class' => 'btn btn-success btn-md control-label']) !!}   
                </div>
            </div>
        
            {!! Form::close() !!}
            <hr>

            {{-- <form action="{{ route('add-blog') }}" method="post" enctype="multipart/form-data">
                <!-- {{csrf_field()}} -->
                @csrf
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" name="title" class="form-control" id="title">

                </div>

                <div class="form-group">
                    <label for="name">Slug</label>
                    <input type="text" name="slug" class="form-control" id="slug" readonly>

                </div>

                <div class="form-group">
                    <label for="name">Content</label>
                    <textarea name="content" id="content" class="form-control" cols="" rows="3"></textarea>

                </div>

                <div class="form-group">
                    <label for="upload">Image</label>
                    <input type="file" name="image" id="upload" class="form-control">
                </div>

                <div class="col-xs-12 form-group">
                    <label for="tags">Tags</label>
                    
                    <select class="form-control select2" multiple="multiple" data- placeholder="Select a State">
                        @foreach ($tags as $key=>$tag)
                            <option value="{{$key}}">{{$tag}}</option>
                            
                        @endforeach
                    </select>
                </div>
                

                <div class="form-group">

                    <input type="submit" value="Add Blog" class="btn btn-success btn-md">

                </div>
            </form> --}}
            <hr>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $('document').ready(function() {

        $(document).off('keyup', '#title').on('keyup', '#title', function() {
            var slug = slugify($(this).val());
            $('#slug').val(slug);
        });

        $('.summernote').summernote({
            tabsize:2,
            height:200,   
            minHeight: null,       
            maxHeight: null,       
            focus: true 
        });

    });

    function slugify(text) {
        return text.toString().toLowerCase()
            .replace(/\s+/g, '-') // Replace spaces with -
            .replace(/[^\w\-]+/g, '') // Remove all non-word chars
            .replace(/\-\-+/g, '-') // Replace multiple - with single -
            .replace(/\_+/g, '-') // Replace _ with -
            .replace(/^-+/, '') // Trim - from start of text
            .replace(/-+$/, ''); // Trim - from end of text
    }
</script>
@endsection