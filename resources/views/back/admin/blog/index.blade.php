@extends('back.layouts.app')

@section('content')

<div class="content">

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <h1> Blogs</h1>
            <hr>

            <a href="{{ route('add-blog') }}" class="btn btn-primary"><span class="title">Add Blog</span></a><p></p>
            <table class="table" id="tagTable">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Blog</th>
                        <th>Description</th>
                        <th>Tags</th>
                        <th>Action</th>
                    </tr>
                </thead>

                @if(count($data)>0)
                @foreach ($data as $key=>$blog)
                
                <tbody>
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$blog->title}}</td>
                        <td>{{$blog->content}}</td>
                        <td>
                            @foreach ($blog->blogTags as $data)
                                <span class="label label-info label-many">{{ $data->title }}</span>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{ route('blog.edit',[$blog->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>

                            {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'DELETE',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['blog.destroy', $blog->id])) !!}
                            {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-default')) !!}
                            {!! Form::close() !!}

                            {!! Form::open(array(
                                'style' => 'display: inline-block;',
                                'method' => 'post',
                                'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                'route' => ['blog.delete', $blog->id])) !!}
                            {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-default')) !!}
                            {!! Form::close() !!}
                            
                        </td>
                    </tr>
                </tbody>
                @endforeach
                @else
                <tr>
                    <td colspan="5">Data Not Found</td>
                </tr>
                @endif
            </table>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#tagTable').DataTable({
            "responsive": true,
            order: [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 75, 100, -1],
                [10, 25, 50, 75, 100, "All"]
            ]
        });
    });
</script>
@endsection