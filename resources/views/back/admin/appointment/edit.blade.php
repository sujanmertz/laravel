@extends('back.layouts.app')

@section('content')

<div class="content">
    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif
    
    <div class="row">
        <div class="col-md-12">
            <h1>Edit Appointment</h1>
            {!! Form::model($appointment, ['method' =>'PUT', 'class' => 'form-horizontal', 'route' => ['appointment.update', $appointment->id]]) !!}
            <div class="form-group">
                {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('contact', 'Contact', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('contact', old('contact'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('date', 'Date', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {{-- {!! Form::date('date', old('date'), ['class' => 'form-control']) !!} --}}
                    <input required class='form-control dates' type="date" name="date" title="Choose your desired date" min="{{ date('Y-m-d') }}" value="{{$appointment->date}}"/>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('time', 'Time', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {{-- {!! Form::time('time', old('time'), ['class' => 'form-control']) !!} --}}
                    {!! Form::select('time', array_merge(array($appointment->time => $appointment->time),$timeslot), old('time'), ['class' => 'form-control']) !!}
                </div>
                
            </div>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    {!! Form::submit('Edit Appointment', ['class' => 'btn btn-success btn-md control-label']) !!}

                </div>
            </div>
            
            
            {!! Form::close() !!}
        </div>
    </div>
</div>

    
@endsection
@section('javascript')
<script type="text/javascript">

    $(document).off('change', '.dates').on('change', '.dates', function(){
            
        var obj = $(this),
            date = obj.val();
            console.log(date);

        $.ajax({
            // headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url  : '/admin/ajax',
            type : "POST",
            data : {_token: "{{ csrf_token() }}",'date':date},

            success: function(resp){
                if (resp.status == 'success') {

                    //obj.parent().parent().siblings().children().find('select[name=time]').html(resp.data);
                    $('select[name=time]').html(resp.data);
                    $('select[name=time]').trigger('change');

                }

            }, error: function() {
                alert('Internal Server Error!!');
            }

        });

            
    });
</script>
    
@endsection