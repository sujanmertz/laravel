@extends('back.layouts.app')

@section('content')
   <div class="content">
        @if(session('success'))
        <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
        @endif

        <div class="row">
            <div class="col-md-12">
                <h1>Appointments</h1>
                <a href="{{ route('appointment.create') }}" class="btn btn-primary"><span class="title">Add Appointment</span></a><p></p>
                <table class="table" id="tagTable">
                    <thead>
                        <tr>
                            <th>S.N.</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Contact</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Action</th>
                        </tr>
                    </thead>
    
                    @if(count($data)>0)
                    @foreach ($data as $key=>$appointment)
                    
                    <tbody>
                        <tr>
                            <td>{{++$key}}</td>
                            <td>{{$appointment->name}}</td>
                            <td>{{$appointment->email}}</td>
                            <td>{{$appointment->contact}}</td>
                            <td>{{$appointment->date}}</td>
                            <td>{{$appointment->time}}</td>
                            <td>
                                <a href="{{ route('appointment.edit',[$appointment->id]) }}" class="btn btn-xs btn-info">@lang('global.app_edit')</a>
    
                                {!! Form::open(array(
                                    'style' => 'display: inline-block;',
                                    'method' => 'DELETE',
                                    'onsubmit' => "return confirm('".trans("global.app_are_you_sure")."');",
                                    'route' => ['appointment.destroy', $appointment->id])) !!}
                                {!! Form::submit(trans('global.app_delete'), array('class' => 'btn btn-xs btn-default')) !!}
                                {!! Form::close() !!}
                                
                            </td>
                        </tr>
                    </tbody>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="5">Data Not Found</td>
                    </tr>
                    @endif
                </table>
            </div>
        </div>
   </div>
@endsection