@extends('back.layouts.app')

@section('content')

<div class="content">
    <div class="row">
        <div class="col-md-12">
            <h1>Add Appointment</h1>
            {!! Form::open(['method' =>'POST', 'class' => 'form-horizontal', 'route' => ['appointment.store']]) !!}
            <div class="form-group">
                {!! Form::label('name', 'Name', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::text('name', old('name'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('email', 'Email', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::email('email', old('email'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('contact', 'Contact', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {!! Form::number('contact', old('contact'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('date', 'Date', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {{-- {!! Form::date('date', old('date'), ['class' => 'form-control','min' => '{{date("Y-m-d")}}']) !!} --}}
                <input required class='form-control dates' type="date" name="date" title="Choose your desired date" min="{{ date('Y-m-d') }}" value="{{date('Y-m-d')}}"/>
                </div>
            </div>
            <div class="form-group">
                {!! Form::label('time', 'Time', ['class' => 'col-sm-2 control-label']) !!}
                <div class="col-sm-6">
                    {{-- {!! Form::time('time', old('time'), ['class' => 'form-control']) !!} --}}
                    {!! Form::select('time', $timeslot, old('time'), ['class' => 'form-control']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-6">
                    {!! Form::submit('Add Appointment', ['class' => 'btn btn-success btn-md control-label']) !!}

                </div>
            </div>
            
            
            {!! Form::close() !!}
        </div>
    </div>
</div>

    
@endsection

@section('javascript')
<script type="text/javascript">

    $(document).off('change', '.dates').on('change', '.dates', function(){
            
        var obj = $(this),
            date = obj.val();
            console.log(date);

        $.ajax({
            // headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
            url  : '/admin/ajax',
            type : "POST",
            data : {_token: "{{ csrf_token() }}",'date':date},

            success: function(resp){
                if (resp.status == 'success') {

                    //obj.parent().parent().siblings().children().find('select[name=time]').html(resp.data);
                    $('select[name=time]').html(resp.data);

                }

            }, error: function() {
                alert('Internal Server Error!!');
            }

        });

            
    });
</script>
    
@endsection