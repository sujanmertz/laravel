@extends('back.layouts.app')

@section('content')

<div class="content">

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif

    <div class="row">
        <div class="col-md-12">
            <h1> @lang('global.app_tags')</h1>
            <hr>

            <a href="{{ route('add-tags') }}" class="btn btn-primary"><span class="title">Add Tag</span></a><p></p>
            <table class="table" id="tagTable">
                <thead>
                    <tr>
                        <th>S.N.</th>
                        <th>Tag</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                </thead>

                @if(count($data)>0)
                @foreach ($data as $key=>$tag)
                <tbody>
                    <tr>
                        <td>{{++$key}}</td>
                        <td>{{$tag->title}}</td>
                        <td>{{$tag->description}}</td>
                        <td>
                            {{--<form action="" method="post">
                                <input type="hidden" name="_uid" value="{{$tag->id}}">
                                {{csrf_field()}}
                                
                                <!-- <input type="submit" class="btn btn-default btn-sm" value=" Delete">
                                <button name="submit" class="btn btn-danger btn-sm"> Delete</button> -->
                                <a href="" class="btn btn-default btn-sm"><i class="glyphicon glyphicon-edit"></i> Edit</a>
                            </form>--}}
                            
                            <form action="{{route('delete')}}" method="post">
                                {{csrf_field()}}
                                <input type="hidden" name="_uid" value="{{$tag->id}}">
                                <a href="{{route('edit',$tag->id)}}" class="btn btn-default btn-sm">Edit</a>
                                <input type="submit" class="btn btn-default btn-sm" value=" Delete" onclick="return confirm('Are you sure you want to delete this item?');">
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
                @else
                <tr>
                    <td colspan="4">Data Not Found</td>
                </tr>
                @endif
            </table>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script type="text/javascript">
    $(document).ready(function() {
        var table = $('#tagTable').DataTable({
            "responsive": true,
            order: [
                [0, 'desc']
            ],
            "lengthMenu": [
                [10, 25, 50, 75, 100, -1],
                [10, 25, 50, 75, 100, "All"]
            ]
        });
    });
</script>
@endsection