@extends('back.layouts.app')

@section('content')

<div class="content">

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <h1>&nbsp;Add New Tag</h1>
            <hr>
            <form action="{{ route('add-tags') }}" method="post" enctype="multipart/form-data">
            <!-- {{csrf_field()}} -->
            @csrf
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" name="title" class="form-control" id="title">

                </div>

                <div class="form-group">
                    <label for="name">Content</label>

                    <textarea name="description" id="description" class="form-control" cols="" rows="5"></textarea>

                </div>
                <div class="form-group">

                    <input type="submit" value="Add Tag" class="btn btn-success btn-md">

                </div>
            </form>
            <hr>
        </div>
    </div>

</div>
@endsection