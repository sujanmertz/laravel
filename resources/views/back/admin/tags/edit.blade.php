@extends('back.layouts.app')

@section('content')

<div class="content">

    @if(session('success'))
    <div class="alert alert-success">{{session('success')}}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>
    @endif

    <div class="row">
        <div class="col-md-6">
            <h1>&nbsp;Edit Tag</h1>
            <hr>
            <form action="{{ route('edit-tag') }}" method="post" enctype="multipart/form-data">
            @csrf
                <input type="hidden" name="id" value="{{$tag->id}}">
                <div class="form-group">
                    <label for="name">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{$tag->title}}">

                </div>

                <div class="form-group">
                    <label for="pass">Description</label>

                    <textarea name="description" id="description" class="form-control" cols="" rows="5">{{$tag->description}}</textarea>

                </div>
                <div class="form-group">

                    <input type="submit" value="Edit Tag" class="btn btn-success btn-md">

                </div>
            </form>
            <hr>
        </div>
    </div>

</div>
@endsection