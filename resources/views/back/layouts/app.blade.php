<!DOCTYPE html>
<html lang="en">

<head>
    @include('back.partials.head')
</head>


<body class="hold-transition skin-blue sidebar-mini">

<div id="wrapper">

@include('back.partials.topbar')
@include('back.partials.sidebar')

<!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
            @if(isset($siteTitle))
                <h3 class="page-title">
                    {{ $siteTitle }}
                </h3>
            @endif

            <div class="row">
                <div class="col-md-12">
                	<div class="panel panel-default">

                    <!-- @if (Session::has('message'))
                        <div class="alert alert-info">
                            <p>{{ Session::get('message') }}</p>
                        </div>
                    @endif -->
                    @if ($errors->count() > 0)
                        <div class="alert alert-default">
                            <ul class="list-unstyled">
                                @foreach($errors->all() as $error)
                                    <li class="alert alert-danger">{{ $error }}<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    @yield('content')
                </div>

                </div>
            </div>
        </section>
    </div>
</div>

@include('back.partials.script')
</body>
</html>