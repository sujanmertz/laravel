<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\User;
use Exception;

// use App\Http\Requests\UserRequest;
// use App\Model\User;
// use Illuminate\Http\Request;
// use App\Http\Controllers\Controller;
// use Illuminate\Support\Facades\Auth;
// use Illuminate\Support\Facades\DB;
// use Mockery\Exception;
// use Image;

class UserController extends Controller
{
    public function login()
    {
        try {
            

        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.auth.login');
        }
    }

    public function loginAction(Request $request)
    {
        $email = $request->email;
        $password = $request->password;

       // var_dump($email,$password);
       // die;
        $remember = isset($request->remember) ? true : false;

        if (Auth::attempt(['email' => $email, 'password' => $password], $remember)) {
            return redirect()->intended('admin');

        } else {
            return redirect()->back()->with('error', 'email or password not valid');
        }
    }

    public function logOut()
    {
        Auth::logout();
        return redirect('login');
    }

    public function setting()
    {
        try {
            $id = Auth::user()->id;
            $users = User::find($id);
            //echo "<pre>";
            //var_dump($users);exit;

        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.settings', compact('users'));
        }
    }

    public function changePassword(Request $request)
    {
        $oldPassword = $request->opassword;
        $id = $request->id;
        $user=User::find($id);
        $userOldPassword=$user->password;
        //var_dump($oldPassword, $id, $userOldPassword);exit;
        //var_dump($request->all());exit;

        $this->validate($request, [
            'opassword' => 'required|old_password:'.$userOldPassword,
            'password' => 'required|min:5|max:100|confirmed',
        ],[
            'opassword.old_password'=>'Old Password does not match' //your own message to be shown during validation
        ]);
//        $data['password']=$request->password;
        $data['password'] = bcrypt($request->password);

        if (User::where('id', $id)->update($data)) {
            return redirect()->route('change-password')->with('success', 'Password was updated successfully');
        }
        return redirect()->back();
    }
}
