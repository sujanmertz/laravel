<?php

namespace App\Http\Controllers\Back;

use App\Model\Blog;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DOMDocument;
use Illuminate\Support\Str;
use Exception;
use Intervention\Image\Facades\Image;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Blog::all();
        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.blog.index', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $tags = \App\Model\Tag::get()->pluck('title', 'id');
        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.blog.create', compact('tags'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'required|mimes:jpeg,png,jpg,gif',
            'tags' => 'required'
        ]);

        $data['title'] = $title = $request->title;
        $data['content'] = $request->content;
        $data['slug'] = Str::slug($title, '-');
        // dd($request->all(), $data);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $uploadPath = public_path('Images/');
            $ext = $image->getClientOriginalExtension();
            $imageName = str_random() . '.' . $ext;

            $make = Image::make($image);

            $save = $make->resize(500, null, function ($ar) {
                $ar->aspectRatio();
            })->crop(450, 350)->save($uploadPath . $imageName);

            if ($save) {
                $data['image'] = $imageName;
            }
        }
        $data['tags'] = implode(",", $request->tags);

        $description = $request->description;

        // $dom = new DOMDocument();
        // $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        // $images = $dom->getElementsByTagName('img');
        // foreach ($images as $k => $img) {
        //     $datas = $img->getAttribute('src');
        //     list($type, $datas) = explode(';', $datas);
        //     list(, $datas)      = explode(',', $datas);
        //     $datas = base64_decode($datas);
        //     $image_name = "/Images/" . time() . $k . '.png';
        //     $path = public_path() . $image_name;
        //     file_put_contents($path, $datas);
        //     $img->removeAttribute('src');
        //     $img->setAttribute('src', $image_name);
        // }

        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($description, 'HTML-ENTITIES', 'UTF-8'),LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');

            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];

                $path = '/Images/' . time() . $count . '.png';

                Image::make($src)
                    ->resize(750, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode($mimeType, 80)
                    ->save(public_path($path));

                $image->removeAttribute('src');
                //$image->setAttribute('src', asset($path));
                $image->setAttribute('src', $path);
            }
        }

        $detail = $dom->saveHTML();
        $data['description'] = $detail;

        $tag = Blog::create($data);
        $tag->blogTags()->attach($request->input('tags'));
        //$tag->blogTags()->sync(array_filter((array)$request->input('tags')));

        return redirect()->route('blogs')->with('success', 'Blog was added successfully');
        // if (Blog::create($data)) {
        //     return redirect()->route('blogs')->with('success', 'Blog was added successfully');
        // } else {
        //     return redirect()->back();
        // }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  integer id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $tags = \App\Model\Tag::get()->pluck('title', 'id');
        //dd($tags);

        $blog = Blog::findOrFail($id);
        //dd($blog->tag->pluck('id'));

        return view('back.admin.blog.edit', compact('blog', 'tags'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  integer id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'image' => 'mimes:jpeg,png,jpg,gif',
            'tags' => 'required'
        ]);

        $data['title'] = $title = $request->title;
        $data['content'] = $request->content;
        $data['slug'] = Str::slug($title, '-');

        $blog = Blog::findorFail($id);
        $oldImage = $blog->image;

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $uploadPath = public_path('Images/');
            $ext = $image->getClientOriginalExtension();
            $imageName = str_random() . '.' . $ext;

            $make = Image::make($image);

            $save = $make->resize(500, null, function ($ar) {
                $ar->aspectRatio();
            })->crop(450, 350)->save($uploadPath . $imageName);

            if ($save) {
                if (!empty($oldImage)) {
                    $image = public_path('Images/' . $oldImage);
                    unlink($image);
                }
                $data['image'] = $imageName;
            }
        }
        $data['tags'] = implode(",", $request->tags);

        $description = $request->description;

        // $dom = new DOMDocument();
        // $dom->loadHtml($description, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        // $images = $dom->getElementsByTagName('img');
        // foreach ($images as $k => $img) {
        //     $datas = $img->getAttribute('src');
        //     list($type, $datas) = explode(';', $datas);
        //     list(, $datas)      = explode(',', $datas);
        //     $datas = base64_decode($datas);
        //     $image_name = "/Images/" . time() . $k . '.png';
        //     $path = public_path() . $image_name;
        //     file_put_contents($path, $datas);
        //     $img->removeAttribute('src');
        //     $img->setAttribute('src', $image_name);
        // }

        // We need this so all weird tags copied from example word are ignored
        // and don' throw exceptions
        libxml_use_internal_errors(true);

        $dom = new DOMDocument();
        $dom->loadHTML(mb_convert_encoding($description, 'HTML-ENTITIES', 'UTF-8'),LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $images = $dom->getElementsByTagName('img');

        foreach ($images as $count => $image) {
            $src = $image->getAttribute('src');

            if (preg_match('/data:image/', $src)) {
                preg_match('/data:image\/(?<mime>.*?)\;/', $src, $groups);
                $mimeType = $groups['mime'];

                $path = '/Images/' . time() . $count . '.png';

                Image::make($src)
                    ->resize(750, null, function ($constraint) {
                        $constraint->aspectRatio();
                    })
                    ->encode($mimeType, 80)
                    ->save(public_path($path));

                $image->removeAttribute('src');
                //$image->setAttribute('src', asset($path));
                $image->setAttribute('src', $path);
            }
        }

        $detail = $dom->saveHTML();
        $data['description'] = $detail;

        $blog->update($data);
        $blog->blogTags()->sync(array_filter((array) $request->input('tags')));

        return redirect()->route('blog.index')->with('success', 'User was updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $blog = Blog::findOrFail($id);
        $image = $blog->image;
        $description =$blog->description;


        if ($blog->delete()) {
            if (!empty($image)) {
                $image = public_path('Images/' . $image);
                unlink($image);
            }

            libxml_use_internal_errors(true);
            $dom = new DOMDocument;
            //$dom->loadHTML(mb_convert_encoding($description, 'HTML-ENTITIES', 'UTF-8'),LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
            $dom->loadHTML(html_entity_decode($description));
            $imagesrc = $dom->getElementsByTagName('img');
            foreach ($imagesrc as $count => $images) {
                $src = public_path($images->getAttribute('src'));
                unlink($src);
                
            }


            $blog->blogTags()->sync(array_filter((array) $blog->tags));

            return redirect()->route('blogs')->with('success', 'Blog was deleted');
        } else {
            return redirect()->back();
        }
    }
}
