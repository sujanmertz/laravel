<?php

namespace App\Http\Controllers\Back;

use App\Model\Appointment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAppointmentRequest;
use App\Http\Requests\UpdateAppointmentRequest;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $data = Appointment::all();
        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.appointment.index', compact('data'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {
            $start_time = "09:00";
            $end_time = "17:00";
            $start_time_str = date("Y/m/d $start_time");
            $end_time_str = date("Y/m/d $end_time");
            //$timeslot = $this->getTimeSlot(30, $start_time_str, $end_time_str);
            // dd($this->getTimeSlots());
            $timeslot = $this->getTimeSlots();
        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.appointment.create', compact('timeslot'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAppointmentRequest $request)
    {
        if (Appointment::create($request->all())) {
            return redirect()->route('appointments')->with('success', 'Appointmnent was added successfully');
        } else {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function show(Appointment $appointment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $start_time = "09:00";
        $end_time = "17:00";
        $start_time_str = date("Y/m/d $start_time");
        $end_time_str = date("Y/m/d $end_time");
        //$timeslot = $this->getTimeSlot(30, $start_time_str, $end_time_str);
        
        $appointment = Appointment::findOrFail($id);
        $timeslot = $this->getTimeSlots($appointment['date']);
        
        return view('back.admin.appointment.edit', compact('appointment', 'timeslot'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAppointmentRequest $request, $id)
    {
        $appointment = Appointment::findOrFail($id);
        if ($appointment->update($request->all())) {
            return redirect()->route('appointments')->with('success', 'Appointment was updated successfully');
        } else {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Appointment  $appointment
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $appointment = Appointment::findorFail($id);

        if ($appointment->delete()) {
            return redirect()->route('appointments')->with('success', 'Appointment was deleted');
        } else {
            return redirect()->back();
            //dd($tag);
        }
    }

    protected function _getTimeSlots()
    {
        $starttime = '9:00';  // your start time
        $endtime = '17:00';  // End time
        $duration = '30';  // split by 30 mins

        $array_of_time = array();
        $start_time    = strtotime($starttime); //change to strtotime
        $end_time      = strtotime($endtime); //change to strtotime
        $start_times    = strtotime($starttime);

        $add_mins  = $duration * 60; // seconds

        while ($start_time < $end_time) // loop between time
        {
            $start_times += $add_mins; // to check endtime
            $array_of_time[] = date("h:i A", $start_time) . ' - ' . date("h:i A", $start_times);
            $start_time = $start_times;
        }

        // return $array_of_time;

        $appointments = Appointment::all();

        // Here I am getting the indexes of the time slot which has appointment
        $indexes_to_be_skipped = array();
        foreach ($appointments as $appointment) {
            for ($i = 0; $i < count($array_of_time); $i++) {
                if ($array_of_time[$i] == $appointment['time']) {
                    $indexes_to_be_skipped[$i] = $i;
                }
            }
        }
        //return $indexes_to_be_skipped;



        $new_array_of_time = array();
        for ($i = 0; $i <= count($array_of_time) - 1; $i++) {
            $new_array_of_time[$array_of_time[$i]] = '' . $array_of_time[$i];

            // check if current time slot has already appointment
            if (isset($indexes_to_be_skipped[$i])) {
                // then remove it
                unset($new_array_of_time[$array_of_time[$i]]);
            }
        }
        return $new_array_of_time;

        // // resetting index
        // $narray_of_time = $new_array_of_time;
        // $new_array_of_time = array();
        // foreach ($narray_of_time as $item) {
        //     $new_array_of_time[] = $item;
        // }

        // return $new_array_of_time;

    }

    protected function getTimeSlots($db_date = null)
    {
        $starttime = '9:00';  // your start time
        $endtime = '17:00';  // End time
        $duration = '30';  // split by 30 mins

        $array_of_time = array();
        $start_time    = strtotime($starttime); //change to strtotime
        $end_time      = strtotime($endtime); //change to strtotime
        $start_times    = strtotime(date("h A", strtotime('+1 hour')));
        $current_time = strtotime(date("h A", strtotime('+1 hour')));

        $add_mins  = $duration * 60; // seconds

        if(is_null($db_date)){
            if ($current_time > $start_time)  {
                $start_times    = strtotime(date("h A", strtotime('+1 hour')));
                $current_time  = strtotime(date("h A", strtotime('+1 hour')));
            } else {
                $start_times    = strtotime($starttime);
                $current_time   = strtotime($starttime);
            }
        }else{
            $start_times    = strtotime($starttime);
            $current_time   = strtotime($starttime);
        }
        
        while ($current_time < $end_time) // loop between time
        {
            $start_times += $add_mins; // to check endtime
            $array_of_time[] = date("h:i A", $current_time) . ' - ' . date("h:i A", $start_times);
            $current_time = $start_times;
        }

        // return $array_of_time;

        $appointments = Appointment::all();

        // Here I am getting the indexes of the time slot which has appointment
        $indexes_to_be_skipped = array();
        foreach ($appointments as $appointment) {
            for ($i = 0; $i < count($array_of_time); $i++) {
                if ($array_of_time[$i] == $appointment['time'] && date('Y-m-d') == $appointment['date']) {
                    $indexes_to_be_skipped[$i] = $i;
                }
            }
        }
        //return $indexes_to_be_skipped;



        $new_array_of_time = array();
        for ($i = 0; $i <= count($array_of_time) - 1; $i++) {
            $new_array_of_time[$array_of_time[$i]] = '' . $array_of_time[$i];

            // check if current time slot has already appointment
            if (isset($indexes_to_be_skipped[$i])) {
                // then remove it
                unset($new_array_of_time[$array_of_time[$i]]);
            }
        }
        return $new_array_of_time;

        // // resetting index
        // $narray_of_time = $new_array_of_time;
        // $new_array_of_time = array();
        // foreach ($narray_of_time as $item) {
        //     $new_array_of_time[] = $item;
        // }

        // return $new_array_of_time;

    }

    function getTimeSlot($duration, $startTime, $endTime)
    {
        $start = new DateTime($startTime);
        $end = new DateTime($endTime);
        $interval = new DateInterval("PT" . $duration . "M");
        $period = new DatePeriod($start, $interval, $end);
        $periods = array();
        $slots = array();
        $slot_counter = 0;
        foreach ($period as $dt) {
            $slots[] = $dt;
        }
        foreach ($slots as $key => $dt) {
            $slot_counter++;
            if ($slot_counter == count($slots)) {
                $current = $end;
            } else if ($slot_counter <= count($slots)) {
                $current = $slots[$key + 1];
            }
            $previous = $slots[$key];
            //$periods[] = array('slot_start_time' => $previous,'slot_end_time' => $current,'slot_timing' => $previous->format('H:i A') . ' - ' . $current->format('H:i A'));
            $periods[$previous->format('H:i A') . ' - ' . $current->format('H:i A')] = $previous->format('H:i A') . ' - ' . $current->format('H:i A');
        }
        return $periods;
    }

    function getAjaxTimeSlot(Request $request)
    {
        if ($request->ajax()) {

            $starttime = '9:00';  // your start time
            $endtime = '17:00';  // End time
            $duration = '30';  // split by 30 mins

            $array_of_time = array();
            $start_time    = strtotime($starttime); //change to strtotime
            $end_time      = strtotime($endtime); //change to strtotime
            $start_times    = strtotime(date("h A"));
            $current_time = strtotime(date("h A"));

            $current_date = $request->date;

            $add_mins  = $duration * 60; // seconds
            if ($current_time > $start_time && $current_date == date('Y-m-d')) {
                $start_times    = strtotime(date("h A", strtotime('+1 hour')));
                $current_time  = strtotime(date("h A", strtotime('+1 hour')));
            } else {
                $start_times    = strtotime($starttime);
                $current_time   = strtotime($starttime);
            }

            while ($current_time < $end_time) // loop between time
            {
                $start_times += $add_mins; // to check endtime
                $array_of_time[] = date("h:i A", $current_time) . ' - ' . date("h:i A", $start_times);
                $current_time = $start_times;
            }

            // return $array_of_time;

            $appointments = Appointment::all();

            // Here I am getting the indexes of the time slot which has appointment
            $indexes_to_be_skipped = array();
            foreach ($appointments as $appointment) {
                for ($i = 0; $i < count($array_of_time); $i++) {
                    if ($array_of_time[$i] == $appointment['time'] && $current_date == $appointment['date']) {
                        $indexes_to_be_skipped[$i] = $i;
                    }
                }
            }
            //return $indexes_to_be_skipped;



            $new_array_of_time = array();
            for ($i = 0; $i <= count($array_of_time) - 1; $i++) {
                $new_array_of_time[$array_of_time[$i]] = '' . $array_of_time[$i];

                // check if current time slot has already appointment
                if (isset($indexes_to_be_skipped[$i])) {
                    // then remove it
                    unset($new_array_of_time[$array_of_time[$i]]);
                }
            }
            $html = null;
            foreach ($new_array_of_time as $key => $detail) {
                $html .= '<option value="' . $detail . '">' . $detail . '</option>';
            }

            return response()->json([
                'status' => 'success',
                'data' => $html
                ]);

        } else {
            return "not found";
        }
    }
}
