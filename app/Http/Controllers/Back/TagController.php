<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\model\Tag;
use Illuminate\Support\Str;
use Exception;

class TagController extends Controller
{
    public function index()
    {
        try {
          $data = Tag::all();

        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.tags.index',compact('data'));
        }
    }

    public function showTag()
    {
        try {

        } catch (Exception $e) {
            die($e->getMessage());
        } finally {
            return view('back.admin.tags.create');
        }
    }

    public function addTagAction(Request $request)
    {

        $data['title'] = $title = $request->title;
        $data['description'] = $request->description;
        $data['slug'] = Str::slug($title, '-');

        //dd($request->all(), $data);

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if (Tag::create($data)) {
            return redirect()->route('tags')->with('success', 'Tag was inserted successfully');
        } else {
            return redirect()->back();
        }
    }

    public function editTag($id)
    {
        try {
            $tag = Tag::find($id);

        } catch (Exception $e) {
            die($e->getMessage());
        }finally
        {
            return view('back.admin.tags.edit', compact('tag'));
        }
    }

    public function editTagAction(Request $request)
    {

        $data['title'] = $title = $request->title;
        $data['description'] = $request->description;
        $data['slug'] = Str::slug($title, '-');
        $id = $request->id;

        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        if (Tag::where('id', $id)->update($data)) {
            return redirect()->route('tags')->with('success', 'Tag was updated successfully');
        } else {
            return redirect()->back();
        }
    }

    public function delete(Request $request)
    {
        $id = $request->_uid;

        $tag = Tag::find($id);

        if ($tag->delete()) {
            return redirect()->route('tags')->with('success', 'Tag was deleted');
        }else {
            return redirect()->back();
            //dd($tag);
        }

    }
}
