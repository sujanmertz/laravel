<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    protected $tables = ['appointment'];
    protected $fillable = ['name','email','contact','date','time'];
}
