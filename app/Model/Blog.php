<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $fillable = ['title', 'slug', 'content', 'image', 'description', 'tags'];

    public function blogTags()
    {
        return $this->belongsToMany(Tag::class, 'blog_tags');
    }
}
